package com.kenfogel.stack_queue_deque_demo;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * The Queue is a First In First Out (FIFO) data structure
 * 
 * In Java a Queue is an interface that is best implemented
 * by an ArrayDeque (Java 1.6)
 * 
 * The interface consists of two sets of three methods.
 * One set throws an exception when it fails, 
 * the other returns special values when it fails
 * 
 * 	Throws Exception		Returns Special Value
 * 	add(e)					offer(e)
 * 	remove()				poll()
 * 	element()				peek()
 * 
 * @author neon
 *
 */
public class BestPracticeQueueExample {
    
    public void perform() {

        // Creating a queue in Java 1.6
        Queue<Integer> queue = new ArrayDeque<>();

        // Add three elements to the queue
        // Autoboxing converts the primitive into to the object type Integer
        queue.offer(7);
        queue.offer(8);
        queue.offer(9);

        // Remove three elements

        // Use isEmpty() to loop thru queue
        while (!queue.isEmpty()) {
            System.out.println("queue poll: " + queue.poll());
        }

        queue.add(3);
        queue.add(4);
        queue.add(5);

        while (!queue.isEmpty()) {
            System.out.println("queue remove: " + queue.remove());
        }
        System.out.println("remove and poll an empty queue");
        // Remove one more element
        System.out.println("queue poll: " + queue.poll());
        System.out.println("queue1 remove: " + queue.remove());
        
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        
        BestPracticeQueueExample bpqe = new BestPracticeQueueExample();
        bpqe.perform();
        System.exit(0);
    }
}
