package com.kenfogel.stack_queue_deque_demo;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * The Deque is a data structure that allows elements to be added
 * or removed from either end
 * 
 * In Java a Deque is an interface that is implemented
 * by a LinkedList and an ArrayDeque (Java 1.6)
 * 
 * 
 * 		First Element (Head) 			Last Element (Tail) 
 * 		Throws exception 	Special value 	Throws exception 	Special value
 * ------------------------------------------------------------------------------ 
 * Insert 	addFirst(e) 		offerFirst(e) 	addLast(e) 		offerLast(e) 
 * Remove 	removeFirst() 		pollFirst() 	removeLast() 		pollLast() 
 * Examine 	getFirst() 		peekFirst() 	getLast() 		peekLast() 
 * 
 * @author neon
 *
 */
public class BestPracticeDequeExample {
    
    public void perform() {
        // Creating a queue in Java 1.6
        Deque<Integer> deque = new ArrayDeque<>();
        
        deque.offerFirst(7);
        deque.offerFirst(8);
        deque.offerFirst(9);
        deque.offerLast(7);
        deque.offerLast(8);
        deque.offerLast(9);

        // Use isEmpty() to loop thru deque

        while (!deque.isEmpty()) {
            System.out.println("deque pollLast: " + deque.pollLast());
        }

        System.out.println("remove and poll an empty deque");
        
        //Remove one more element
        //Returns null if there are no more elements
        System.out.println("deque poll: " + deque.pollLast());
        // Throws exception if there are no more elements
        System.out.println("deque poll: " + deque.removeLast());        
        
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        
        BestPracticeDequeExample bpde = new BestPracticeDequeExample();
        bpde.perform();
        System.exit(0);

    }
}
