package com.kenfogel.stack_queue_deque_demo;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * The Stack is a Last In First Out (LIFO) data structure
 * 
 * A Stack should implement:
 * 		push	add to the stack
 * 		pop	remove from the stack
 * 		peek	look at but not remove the top of the stack
 * 		empty	true if stack is empty
 * 
 * @author neon
 *
 */
public class BestPracticeStackExample {

    public void perform() {

        // The preferred way to implement a stack in Java 1.6
        // uses an ArrayDeque
        Deque<Integer> stack = new ArrayDeque<>();

        stack.push(9);
        stack.push(31);
        System.out.println("stack peek: " + stack.peek());

        int x;
        
        System.out.println("stack empty before pop: " + stack.isEmpty());
        while (!stack.isEmpty()) {
            x = stack.pop();
        }
        System.out.println("stack empty after pop: " + stack.isEmpty());

        System.out.println("Pop an empty stack");
        System.out.println("stack");
        // Always check if a stack is empty before you pop otherwise . . .
        x = stack.pop();
        
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        BestPracticeStackExample bpse = new BestPracticeStackExample();
        bpse.perform();
        System.exit(0);
    }
}
